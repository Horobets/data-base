from db.connection import Connection

from sources.korrespondent import Korrespondent
from sources.hromadske import Hromadske
from sources.pravda import Pravda

# Визначення провайдерів
providers = {
  'korrespondent': Korrespondent(),
  'hromadske': Hromadske(),
  'pravda': Pravda()
}

# Під'єднання до БД
con = Connection()
data = con.execute('SELECT id, name, provider FROM sources;')
sql = "INSERT INTO posts(title, content, created_at, uuid, source_id) VALUES (%s, %s, %s, %s, %s) ON CONFLICT (uuid, source_id) DO NOTHING;"

# Імпорт новин з джерел
for row in data:
  provider = providers.get(row[2], None)

  if provider is not None:
    for post in provider.all():
      con.insert(sql,
        (
          post['title'],
          post['content'],
          post['date'],
          post['uuid'],
          row[0]
        ))

    count = con.count('SELECT COUNT(*) FROM posts WHERE state = 0 AND source_id = ' + str(row[0]))
    print(row[1] + " imported news: " + str(count))


# Від'єднання від БД
con.close()

print("Done.")
