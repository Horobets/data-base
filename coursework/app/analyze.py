from db.connection import Connection

import spacy
import pytextrank

nlp = spacy.load("xx_ent_wiki_sm")

tr = pytextrank.TextRank()
nlp.add_pipe(nlp.create_pipe('sentencizer'))
nlp.add_pipe(tr.PipelineComponent, name="textrank", last=True)
nlp.max_length = 10**7

# Під'єднання до бази даних
con = Connection()
data = con.execute('SELECT id, title, content FROM posts WHERE state = 0;')

# Обробка новин
for row in data:
  doc = nlp(row[2])
  post_id = row[0]
  data = list()

  for phrase in doc._.phrases[:20]:
    items = [str(item).strip() for item in phrase.chunks]

    data.append(items)

  if len(data) > 0:
    # Знищення дублікатів новин, якщо такі є
    tags = list(dict.fromkeys(data[0]))

    print(tags)

    con.update('UPDATE posts SET state = 1, tags = %s WHERE id = %s', (tags, post_id))

print('Done.')
