import os
import yaml
import psycopg2

class Connection:
  def __init__(self):
    self.conn = None
    self.config_path = 'config/database.yml'

  def connect(self):
    options = self.config()

    self.conn = psycopg2.connect(**options['postgres'])

  def close(self):
    if self.conn is not None:
      self.conn.close()

  def insert(self, sql, values):
    if self.conn is None:
      self.connect()

    cur = self.conn.cursor()
    cur.execute(sql, values)
    self.conn.commit()

    cur.close()

  def update(self, sql, values):
    if self.conn is None:
      self.connect()

    cur = self.conn.cursor()
    cur.execute(sql, values)
    rowcount = cur.rowcount
    self.conn.commit()

    cur.close()

    return rowcount

  def execute(self, sql):
    if self.conn is None:
      self.connect()

    cur = self.conn.cursor()
    cur.execute(sql)
    data = cur.fetchall()

    cur.close()

    return data

  def count(self, sql):
    if self.conn is None:
      self.connect()

    cur = self.conn.cursor()
    cur.execute(sql)
    data = cur.fetchone()

    cur.close()

    return int(data[0])

  def config(self):
    data = ''
    root_dir = self.path_to_root_dir()
    path = os.path.join(root_dir, self.config_path)

    with open(path, 'r') as stream:
      data = yaml.safe_load(stream)

    return data

  def path_to_root_dir(self):
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    app_dir = os.path.dirname(curr_dir)

    return os.path.dirname(app_dir)
