import json
from sources.base import Base

class Pravda(Base):
  domain = "https://www.pravda.com.ua"

  def __init__(self):
    self.url = self.domain + "/news"

  def links(self):
    doc = self.document(self.url)

    return doc.select('div.article_news_list div.article_header a[href]')

  def all(self):
    for link in self.links():
      post = self.parse(self.domain + link.get('href'))

      if post is not None:
        yield post

  def parse(self, url):
    doc = self.document(url)
    data = {
      "title": None,
      "content": None,
      "date": None,
      "uuid": url.split('/')[-2]
    }

    root = doc.find('article')

    if root is None:
      return None

    data["title"] = root.find('h1').text.strip()
   
    node = root.find('div', attrs={"class": "post__text"})
    if node is None:
      node = root.find('div', attrs={"class": "post_text"})

    data["content"] = node.text.strip()

    metadata = doc.find("script", {"type": "application/ld+json"})
    metadata = json.loads(metadata.string)

    data["date"] = metadata['datePublished']

    return data
