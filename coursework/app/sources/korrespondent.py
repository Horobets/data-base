import json
from sources.base import Base

class Korrespondent(Base):
  def __init__(self):
    self.url = "https://ua.korrespondent.net/all/"

  def links(self):
    doc = self.document(self.url)

    return doc.select('div.articles-list div.article__title h3 a[href]')

  def all(self):
    for link in self.links():
      post = self.parse(link.get('href'))

      if post is not None:
        yield post

  def parse(self, url):
    doc = self.document(url)
    data = {
      "title": None,
      "content": None,
      "date": None,
      "uuid": url.split('/')[-1]
    }

    root = doc.find('div', attrs={"class": "post-item"})

    if root is None:
      return None

    data["title"] = root.find('h1', attrs={"class": "post-item__title"}).text.strip()

    data["content"] = root.find('div', attrs={"class": "post-item__text"}).text.strip()

    metadata = doc.find("script", {"type": "application/ld+json"})
    metadata = json.loads(metadata.string)

    data["date"] = metadata['datePublished']

    return data
