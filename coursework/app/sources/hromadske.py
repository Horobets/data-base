from sources.base import Base

class Hromadske(Base):
  domain = "https://hromadske.ua"

  def __init__(self):
    self.url = self.domain + "/news"

  def links(self):
    doc = self.document(self.url)

    return doc.select('div.NewsPostList a[href]')

  def all(self):
    for link in self.links():
      post = self.parse(self.domain + link.get('href'))

      if post is not None:
        yield post

  def parse(self, url):
    doc = self.document(url)
    data = {
      "title": None,
      "content": None,
      "date": None,
      "uuid": url.split('/')[-1]
    }

    root = doc.find('article', attrs={"class": "PostPreview"})

    if root is None:
      return None

    data["title"] = root.find('h1', attrs={"class": "PostHeader-title"}).text.strip()

    data["content"] = root.find('div', attrs={"class": "PostContent-body"}).text.strip()

    data["date"] = root.find('div', attrs={"itemprop": "datePublished"}).get('content')

    return data
