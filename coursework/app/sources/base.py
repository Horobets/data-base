import requests
from bs4 import BeautifulSoup

class Base:
  def load_html(self, url):
    try:
      data = requests.get(url).text
    except:
      data = ''

    return data

  def document(self, url):
    html = self.load_html(url)
    return BeautifulSoup(html, 'html.parser')
