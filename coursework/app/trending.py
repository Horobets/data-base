from db.connection import Connection

sql = "SELECT unnest(tags), count(*) AS total_count from posts GROUP BY unnest(tags) ORDER BY count(*) desc LIMIT 30"

con = Connection()
data = con.execute(sql)

for row in data:
  print(row)
