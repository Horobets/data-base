from db.connection import Connection

con = Connection()

# Фільтр новин за source_id:
# 1 - Korrespondent
# 2 - Hromadske
# 3 - Українська правда
#
sql = "SELECT title, created_at from posts WHERE source_id = 2 ORDER BY created_at DESC LIMIT 20"
data = con.execute(sql)


# Пошук новин за текстом
text = input("Текст для пошуку: ")
sql = "SELECT title, created_at from posts WHERE title ILIKE '%" + text + "%' ORDER BY created_at DESC LIMIT 20"
data = con.execute(sql)

print("Результат пошуку за ключовим словом: " + text)
for row in data:
  print(row)
