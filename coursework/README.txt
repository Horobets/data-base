## Збір програми


docker-compose up --build


## Створити базу даних (запускається один раз)


./bin/run.sh
./bin/setup.sh


## Запуск програми


./bin/run.sh

python3 app/import.py

python3 app/analyze.py

python3 app/trending.py

python3 app/search.py


## PG Admin

http://localhost:8080

## Бекап бази даних

./bin/run.sh
./bin/backup.sh
