#!/bin/sh

set -e
LC_ALL=C
LANG=C

GREEN='\033[0;32m'
NC='\033[0m' # No Color

# Бекап
echo -e "${GREEN}Backup DataBase news_analyst to db/news_analyst.dump${NC}"
pg_dump -h postgres-master -p 5432 -U postgres news_analyst > db/news_analyst.dump

echo -e "${GREEN}Done.${NC}"
