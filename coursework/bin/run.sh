#!/bin/sh

set -e
LC_ALL=C
LANG=C

# Збір
docker-compose build news_analyst

# Запуск
docker-compose run news_analyst sh
