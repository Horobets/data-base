#!/bin/sh

set -e
LC_ALL=C
LANG=C

GREEN='\033[0;32m'
NC='\033[0m' # No Color

# Створення бази даних
echo -e "${GREEN}Create DataBase (drop if already exists)${NC}"
psql -h postgres-master -p 5432 -U postgres < ./db/create.sql

# Створеення таблиць
echo -e "${GREEN}Create tables${NC}"
psql -h postgres-master -p 5432 -U postgres news_analyst < ./db/structure.sql

# Внесення джерел
echo -e "${GREEN}Insert sources${NC}"
psql -h postgres-master -p 5432 -U postgres news_analyst < ./db/data.sql

echo -e "${GREEN}Done.${NC}"
