SET client_encoding = 'UTF8';

--
-- Name: sources_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sources; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sources (
    id integer NOT NULL DEFAULT nextval('sources_id_seq'),
    name character varying(150),
    provider character varying(50)
);


--
-- Name: sources_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sources_id_seq OWNED BY public.sources.id;

--
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: posts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.posts (
    id integer NOT NULL DEFAULT nextval('posts_id_seq'),
    title text,
    content text,
    state integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    tags character varying(255)[] DEFAULT '{}'::character varying[],
    source_id integer NOT NULL,
    uuid character varying(200) NOT NULL
);


--
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.posts_id_seq OWNED BY public.posts.id;


--
-- Name: index_posts_on_source_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_posts_on_source_id ON public.posts USING btree (source_id);
CREATE UNIQUE INDEX index_posts_on_uuid ON public.posts USING btree (uuid, source_id);
