SET client_encoding = 'UTF8';

DROP DATABASE IF EXISTS news_analyst;

CREATE DATABASE news_analyst WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';
