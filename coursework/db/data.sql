SET client_encoding = 'UTF8';

INSERT INTO sources (id, name, provider) VALUES
  (1, 'Korrespondent', 'korrespondent'),
  (2, 'Hromadske', 'hromadske'),
  (3, 'Українська правда', 'pravda');
