import psycopg2


class Model(object):
    def __init__(self):
        try:
            self.connection = psycopg2.connect(
                host="127.0.0.1",
                database="labs",
                user="postgres",
                password="biedronokpost"
            )
            self.cursor = self.connection.cursor()
        except(Exception, psycopg2.Error):
            print('Error during connection')

    def find_email(self, text):
        try:self.cursor.execute("SELECT * FROM public.\"email\" WHERE text ILIKE '%" + text + "%'")

        except (Exception, psycopg2.Error) as error :
            print ("Error while fetching data from PostgreSQL", error)
        
        finally: return self.cursor.fetchall()

    def get_user(self, id):
        try:self.cursor.execute("SELECT * FROM public.\"user\" WHERE id = %s" % id)

        except (Exception, psycopg2.Error) as error :
            print ("Error while fetching data from PostgreSQL", error)
        
        finally: return self.cursor.fetchall()

    def delete_user(self, id):
        self.cursor.execute("DELETE FROM public.\"user\" WHERE id = %s" % id)
        self.cursor.execute("UPDATE \"email\" SET user_id = 0 WHERE user_id = %s" % id)
        self.cursor.execute("UPDATE \"email\" SET to_id = 0 WHERE to_id = %s" % id)
        self.connection.commit()

    def get_users(self):
        self.cursor.execute("SELECT * FROM public.\"user\"")
        return self.cursor.fetchall()
    
    def add_user(self, user):
        postgres_insert_query = """ INSERT INTO public.\"user\" (name, password) VALUES (%s,%s)"""
        self.cursor.execute(postgres_insert_query, user)
        self.connection.commit()



    def get_emails(self):
        self.cursor.execute("SELECT * FROM public.\"email\"")
        return self.cursor.fetchall()

    def get_email(self, id):
        self.cursor.execute("SELECT * FROM public.\"email\" WHERE id = %s" % id)
        return self.cursor.fetchall()
    
    def delete_email(self, id):
        self.cursor.execute("DELETE FROM public.\"email\" WHERE id = %s" % id)
        self.connection.commit()

    def add_email(self, email):
        postgres_insert_query = """ INSERT INTO public.\"email\" (text, to_id, user_id, folder_id, read) VALUES (%s,%s,%s,%s,%s)"""
        self.cursor.execute(postgres_insert_query, email)
        self.connection.commit()



    def get_folders(self):
        self.cursor.execute("SELECT * FROM public.\"folder\"")
        return self.cursor.fetchall()

    def get_folder(self, id):
        self.cursor.execute("SELECT * FROM public.\"folder\" WHERE id = %s" % id)
        return self.cursor.fetchall()
    
    def delete_folder(self, id):
        self.cursor.execute("DELETE FROM public.\"folder\" WHERE id = %s" % id)
        self.cursor.execute("UPDATE \"email\" SET folder_id = 0 WHERE folder_id = %s" % id)
        self.connection.commit()

    def add_folder(self, folder):
        postgres_insert_query = """ INSERT INTO public.\"folder\" (name, description) VALUES (%s,%s)"""
        self.cursor.execute(postgres_insert_query, folder)
        self.connection.commit()


    
    def generate_user(self, amount):
        self.cursor.execute("INSERT INTO \"user\"(name, password) SELECT random_string(1+(random()*7)::int), random_string(1+(random()*10)::int) FROM generate_series(1,%s)" % amount)
        self.connection.commit()
    
    def generate_folder(self, amount):
        self.cursor.execute("INSERT INTO \"folder\"(name, description) SELECT random_string(1+(random()*7)::int), random_string(1+(random()*10)::int) FROM generate_series(1,%s)" % amount)
        self.connection.commit()

    def generate_email(self, amount):
        self.cursor.execute("INSERT INTO \"email\"(text, to_id, user_id, folder_id, read) SELECT (random_string(1+(random()*20)::int)),(pick_random_user_id()),(pick_random_user_id()),(pick_random_folder_id()),(random_boolean()) FROM generate_series(1,%s)" % amount)
        self.connection.commit()



    def scene1(self):
        try:self.cursor.execute("SELECT fl.id, e.user_id, COUNT(*) as c FROM \"folder\" fl INNER JOIN \"email\" e ON e.folder_id = fl.id GROUP BY e.user_id, fl.id HAVING COUNT(*) = (SELECT MAX(c) as m FROM (SELECT fl.id, e.user_id, COUNT(*) as c FROM \"folder\" fl INNER JOIN \"email\" e ON e.folder_id = fl.id GROUP BY e.user_id, fl.id) as k)")

        except (Exception, psycopg2.Error) as error :
            print ("Error while fetching data from PostgreSQL", error)
        finally:
            return self.cursor.fetchall()
    

    def scene2(self):
        try:self.cursor.execute("SELECT e.text, f.c FROM \"email\" e LEFT JOIN (SELECT e.user_id, COUNT(*) as c FROM \"email\" e GROUP BY e.user_id) f ON e.user_id = f.user_id where f.c>=ALL(SELECT COUNT(*) as c FROM \"email\" e GROUP BY e.user_id)")

        except (Exception, psycopg2.Error) as error :
            print ("Error while fetching data from PostgreSQL", error)
        finally:
            return self.cursor.fetchall()