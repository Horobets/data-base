class Controller:
    def __init__(self, view, model):
        self.view = view
        self.model = model



    def get_user(self):
        id = self.view.get_id()
        user = self.model.get_user(id)
        self.view.print(user)
    
    def show_user(self):
        self.view.print(self.model.get_users())

    def delete_user(self):
        id = self.view.get_id()
        user = self.model.get_user(id)
        self.view.deleted(user)
        self.model.delete_user(id)

    def add_user(self):
        user = self.view.add_user()
        self.model.add_user(user)



    def get_email(self):
        id = self.view.get_id()
        email = self.model.get_email(id)
        self.view.print(email)

    def show_email(self):
        self.view.print(self.model.get_emails())

    def delete_email(self):
        id = self.view.get_id()
        email = self.model.get_email(id)
        self.view.deleted(email)
        self.model.delete_email(id)
        
    def add_email(self):
        email = self.view.add_email()
        self.model.add_email(email)

        

    def get_folder(self):
        id = self.view.get_id()
        folder = self.model.get_folder(id)
        self.view.print(folder)

    def show_folder(self):
        self.view.print(self.model.get_folders())

    def delete_folder(self):
        id = self.view.get_id()
        folder = self.model.get_folder(id)
        self.view.deleted(folder)
        self.model.delete_folder(id)
        
    def add_folder(self):
        folder = self.view.add_folder()
        self.model.add_folder(folder)



    def generate_user(self):
        amount = self.view.get_amount()
        self.model.generate_user(amount)

    def generate_folder(self):
        amount = self.view.get_amount()
        self.model.generate_folder(amount)

    def generate_email(self):
        amount = self.view.get_amount()
        self.model.generate_email(amount)



    def find_email(self):
        text = self.view.get_text()
        self.view.print(self.model.find_email(text))



    def scene1(self):
        print("(folder_id, user_id, amount of emails)")
        self.view.print(self.model.scene1())

    def scene2(self):
        print("(text of email, amount of letters written by author)")
        self.view.print(self.model.scene2())






