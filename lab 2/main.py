from controller import Controller
from model import Model
from view import View

ctrl = Controller(View(), Model())

print("Commands:\n")
print("1.show_user - вивести список користувачів (delete/add/get перед '_' ) \n")
print("2.show_email - вивести список листів (delete/add перед/get '_' ) \n")
print("3.show_folder - вивести список користувачів (delete/add перед/get '_' ) \n")
print("4.find_email - знайти повідомлення за набором символів з його тексту \n")
print("5.find_1 -  список папок, де зберігаються найбільша кількість повідомлень від одного користувача\n")
print("6.find_2 -  список повідомлень від користувачів, які найчастіше відправляли повідомлення\n")
print("7. generate_ - згенерувати випадкові дані (назва папки після _ ) \n")
print("8. end \n")

while True:
    
    cmd = input("Your command: ")
    
    if cmd == "get_user":
        ctrl.get_user()
    elif cmd == "show_user":
        ctrl.show_user()
    elif cmd == "delete_user":
        ctrl.delete_user()
    elif cmd == "add_user":
        ctrl.add_user()


    elif cmd == "get_email":
        ctrl.get_email()
    elif cmd == "show_email":
        ctrl.show_email()
    elif cmd == "delete_email":
        ctrl.delete_email()
    elif cmd == "add_email":
        ctrl.add_email()


    elif cmd == "get_folder":
        ctrl.get_folder()
    elif cmd == "show_folder":
        ctrl.show_folder()
    elif cmd == "delete_folder":
        ctrl.delete_folder()
    elif cmd == "add_folder":
        ctrl.add_folder()


    elif cmd == "find_email":
        ctrl.find_email()
    elif cmd == "find_1":
        ctrl.scene1()
    elif cmd == "find_2":
        ctrl.scene2()


    elif cmd == "generate_email":
        ctrl.generate_email()
    elif cmd == "generate_folder":
        ctrl.generate_folder()
    elif cmd == "generate_user":
        ctrl.generate_user()


    elif cmd == "end":
        break
    else:
        print("\nwrong command!\n")
