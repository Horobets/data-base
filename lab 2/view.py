
class View:
    @staticmethod
    def get_text():
        return input("text from email: ")

    @staticmethod
    def get_id():
        return input("id is: ")

    @staticmethod
    def get_amount():
        return input("amount is: ")

    @staticmethod
    def print(something):
        if len(something) > 0:
            for r in something:
                print(r)
        else:
            print("--empty--")
    
    @staticmethod
    def deleted(something):
        if len(something) > 0:
            for r in something:
                print(r)
            print("\n--DELETED--\n")

    @staticmethod
    def add_user():
        user = []
        for x in range(0,2):
            if x == 0:
                user.append(input("insert name: "))
            else:
                user.append(input("insert password: "))

        return user

    @staticmethod
    def add_email():
        email = []
        for x in range(0,5):
            if x == 0:
                email.append(input("insert text: "))
            if x == 1:
                email.append(input("insert to_id: "))
            if x == 2:
                email.append(input("insert user_id: "))
            if x == 3:
                email.append(input("insert folder_id: "))
            if x == 4:
                i = input("insert if it's read: ")
                if i == "true":
                    email.append(i)
                if i == "false":
                    email.append(i)
        print(email)
        return email

    @staticmethod
    def add_folder():
        folder = []
        for x in range(0,2):
            if x == 0:
                folder.append(input("insert name: "))
            else:
                folder.append(input("insert description: "))

        return folder